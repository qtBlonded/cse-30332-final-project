function postInfo() {
  var cardName = document.getElementById("image").getAttribute("value");
  document.getElementById("recruit").textContent = "This card's name is: " + cardName;
}

function newCard(){
    var url = "http://localhost:51054/cards/random";
    var url2 = "https://api.scryfall.com/cards/";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        var obj = JSON.parse(xhr.responseText);
        var id = obj.value.identifiers.scryfallId;
        
        var xhr2 = new XMLHttpRequest();
        xhr2.open("GET", url2+id, true);

        xhr2.onload = function(e) {
            console.log(xhr2.responseText);
            var obj2 = JSON.parse(xhr2.responseText);
            var image = obj2.image_uris.art_crop;
            var name = obj2.name;

            document.getElementById("image").setAttribute("value", name);
            document.getElementById("image").setAttribute("src", image);
            document.getElementById("recruit").textContent = "";
        }
        xhr2.onerror = function(e) {
            console.error(xhr2.statusText);
        }
        xhr2.send(null);
        }
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }
    xhr.send(null);
    
}

function showAnswer(){
    var label1 = document.getElementById("radios-text");
    var action = "radio1";
    if (document.getElementById("bsr-radio1").checked){
        action = "radio1";
    } else if (document.getElementById("bsr-radio2").checked){
        action = "radio2";
    }else if (document.getElementById("bsr-radio3").checked){
        action = "radio3";
    }
    if (action == "radio3"){ //correct){
        label1.innerHTML = "That is the correct answer!";
    }
    else {
        label1.innerHTML = "That is not the correct answer!";
    }
}
