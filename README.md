#Final Project CSE 30332

GET, /cards/, "", json of all cards


GET, /cards/:key, "", json of a card


GET, /cards/random, "", json representing a random card


PUT, /cards/:key, json of a card, json representing success


POST, /cards/, json of all cards, json representing success


DELETE, /cards/:key, "", json representing success


DELETE, /cards/, "", json representing success


#Server location
On the student machines here: /escnfs/courses/sp21-cse-30332.01/dropbox/qbardwel/complex-server
Run with: python3 ./server.py


#Final Prensentation Slides
https://docs.google.com/presentation/d/1ofz-qTq6nl21ZBvdR5PmcdNf7mJmoKNbffX7Gxg77to/edit?usp=sharing


#Complexity

The complexity of this project is mild. There aren't many individual components of high complexity. Rather there a a handful of decently challenging sub-problems. For example, the REST API is one of these and is probably the hardest part. After that it would be managing the JS calls in HTML, then the JS making API calls. All in all, there are a couple of tricky things that had to be done and a lot of the challenges came from the inexperience of putting together a server and client. 
